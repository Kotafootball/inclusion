program main
use module
use module2
implicit none

integer j,e,pn,ep,f1,bc0,k,l,g
integer,allocatable::globalnum(:,:),bc(:,:)
real(8) r,det,ec,em,sita,a,c,a1,b1,y1,y2,xi,yi,p1,p2,b,gzi,ita,nn1,nn2,nn3,nn4,xxx,yyy,el22,eem,eec
real(8) jm(2,2),lamda1,lamda2,mu1,mu2,alpha,uthr,uthx,uthy,uxfem,uyfem
real(8),allocatable:: n(:,:),u(:),bc1(:),el2(:)
eem = 0.0d0
eec = 0.0d0
call read_file1(y1,p1,y2,p2,a1,b) 
call read_file2(pn, n)
call read_file3(e, ep, globalnum) 
call read_file4(bc0 ,f1, bc, bc1)
call read_file6(u,pn,f1)
call uthdata(b,a1,y1,y2,p1,p2,b1,alpha)
allocate(el2(e),source = 0.0d0)
do i = 1, e  !要素ループ
   ec = 0.0d0
   em = 0.0d0
   do j = 1,4  !積分点ループ
      call gziita(j,c,a,nn1,nn2,nn3,nn4)
      xi = (nn1*n(globalnum(i,1),1) + nn2*n(globalnum(i,2),1) + &
            nn3*n(globalnum(i,3),1) + nn4*n(globalnum(i,4),1)) / 4.0d0
      yi = (nn1*n(globalnum(i,1),2) + nn2*n(globalnum(i,2),2) + &
            nn3*n(globalnum(i,3),2) + nn4*n(globalnum(i,4),2)) / 4.0d0
      call uth(a1,b1,alpha,xi,yi,uthx,uthy)
      uxfem = (nn1*u(globalnum(i,1)*2-1) + nn2*u(globalnum(i,2)*2-1) + &  !!積分点変位補完
               nn3*u(globalnum(i,3)*2-1) + nn4*u(globalnum(i,4)*2-1)) / 4.0d0
      uyfem = (nn1*u(globalnum(i,1)*2) + nn2*u(globalnum(i,2)*2) + &
               nn3*u(globalnum(i,3)*2) + nn4*u(globalnum(i,4)*2)) / 4.0d0

   call jmat(jm,c,a,n,globalnum,i)

      det = ((jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1))) 
      ec = ec + ((uthx - uxfem) ** 2.0d0 + (uthy - uyfem) ** 2.0d0) * det  
      em = em + (uthx ** 2.0d0 + uthy ** 2.0d0) * det
      eec = eec + ((uthx - uxfem) ** 2.0d0 + (uthy - uyfem) ** 2.0d0) * det  
      eem = eem + (uthx ** 2.0d0 + uthy ** 2.0d0) * det
   end do
   el2(i) = sqrt(ec) / sqrt(em)
end do
el22 = sqrt(eec) / sqrt(eem)
write(*,*) "el22", el22
call print_text2(g,ep,e,pn,globalnum,n,el2)
end program