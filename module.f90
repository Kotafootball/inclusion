module module
  implicit none
  integer i, o, q, w

contains
  
  subroutine read_file1(y1,p1,y2,p2,a1,b1)
    implicit none
    real(8) y1, p1, y2, p2, a1, b1
    open(10, file="inputinclusion.dat", status="old")
    read (10, *) y1, p1
    read (10, *) y2,p2
    read(10,*) a1, b1
    close (10)
  end subroutine read_file1
  
  subroutine read_file2(pn,n)
    implicit none
    integer pn
    real(8),allocatable::n(:,:)
    open(11, file="nodeinclusion.dat", status="old")
    read (11, *) pn
    allocate(n(pn,2))
    do i = 1, pn
       read(11,*) n(i,1), n(i,2)
    end do
    close (11)
  end subroutine read_file2

  subroutine read_file3(e,ep,globalnum)
    implicit none
    integer e, ep
    integer,allocatable::globalnum(:,:)
    open(12, file="eleminclusion.dat", status="old")
    read (12, *)e, ep
    allocate (globalnum(e,ep), source = 0)
    do i = 1, e
       read(12,*)globalnum(i,1), globalnum(i,2), globalnum(i,3),globalnum(i,4)
    end do
    close (12)
  end subroutine read_file3

  subroutine read_file4(bc0,f1,bc,bc1)
    implicit none
    integer bc0, f1
    integer,allocatable::bc(:,:)
    real(8),allocatable::bc1(:)
    open(13, file="bcinclusion.dat", status="old")
    read (13, *) bc0, f1
    allocate(bc(bc0,2), source = 0)
    allocate(bc1(bc0), source = 0.0d0)
    do i = 1, bc0
       read (13, *)bc(i,1), bc(i,2), bc1(i)
    end do
    close (13)
  end subroutine read_file4

  subroutine read_file5(ld0,f2,ld,ldf)
    implicit none
    integer ld0, f2
    integer,allocatable::ld(:,:)
    real(8),allocatable::ldf(:)
    open(14, file="load2.dat", status="old")
    read (14, *)ld0, f2
    allocate(ld(ld0,2), source = 0)
    allocate(ldf(ld0), source = 0.0d0)
    do i = 1, ld0
       read(14, *) ld(i,1), ld(i,2), ldf(i)
    end do
    close (14)
  end subroutine read_file5

  subroutine read_file6(u,pn,f1)
    implicit none
    integer pn,f1,Nn
    real(8),allocatable:: u(:)

    Nn = pn * f1
    allocate(u(Nn), source = 0.0d0)
    open(15, file="dispinclusion.dat", status="old")
    do i = 1, Nn
       read(15,*)u(i)
    end do
    close (15)
  end subroutine read_file6

  subroutine print_text(g,ep,e,pn,globalnum,n,u,eps,sgm)
    implicit none
    integer g, ep, e
    integer pn
    integer,allocatable::globalnum(:,:)
    real(8),allocatable::n(:,:),u(:),eps(:,:),sgm(:,:)
    g = (1 + ep) * e
    open(30, file = "exercise.vtk", status = "replace")
    write(30,"(a)")"# vtk DataFile Version 4.1"
    write(30,"(a)")"this is command line"
    write(30,"(a)")"ASCII"
    write(30,"(a)")"DATASET UNSTRUCTURED_GRID"
    write(30,*)" "
    write(30,"(a,i8,1x,a)")trim("POINTS"), pn, trim("float")
    do i = 1, pn
       write(30,"(f15.8,1x,f15.8,1x,f15.8)") n(i,1),n(i,2), 0.0
    end do
    write(30,*)" "
    write(30,"(a,i8,i8)")trim("CELLS"),e,g
    do i = 1, e
       write(30,*)ep,globalnum(i,1) - 1, globalnum(i,2) - 1, globalnum(i,3) - 1, globalnum(i,4) - 1
    end do
    write(30,*)" "
    write(30,"(a,i8)")trim("CELL_TYPES"),e
    do i = 1, e
       write(30,*)"9"
    end do

    write(30,*)" "
    write(30,"(a,i6)")trim("POINT_DATA"),pn
    write(30,"(a,1x,a,1x,a)")trim("VECTORS"),trim("Displacement"),trim("float")
    do i = 1 , pn
      write(30,*)u(2 * i - 1), u(2 * i), 0.0d0
    enddo 

    close (30)
  end subroutine print_text

  subroutine print_text2(g,ep,e,pn,globalnum,n,el2)
    implicit none
    integer g, ep, e, i, pn
    integer,allocatable::globalnum(:,:)
    real(8),allocatable::n(:,:),u(:),eps(:,:),sgm(:,:),el2(:)
    g = (1 + ep) * e
    open(30, file = "el2.vtk", status = "replace")
    write(30,"(a)")"# vtk DataFile Version 4.1"
    write(30,"(a)")"this is command line"
    write(30,"(a)")"ASCII"
    write(30,"(a)")"DATASET UNSTRUCTURED_GRID"
    write(30,*)" "
    write(30,"(a,i8,1x,a)")trim("POINTS"), pn, trim("float")
    do i =1, pn
       write(30,"(f15.8,1x,f15.8,1x,f15.8)") n(i,1),n(i,2), 0.0
    end do
    write(30,*)" "
    write(30,"(a,i8,i8)")trim("CELLS"),e,g
    do i = 1, e
       write(30,*)ep,globalnum(i,1) - 1, globalnum(i,2) - 1, globalnum(i,3) - 1, globalnum(i,4) - 1
    end do
    write(30,*)" "
    write(30,"(a,i8)")trim("CELL_TYPES"),e
    do i = 1, e
       write(30,*)"9"
    end do

    write(30,*)" "
    write(30,"(a,i6)")trim("CELL_DATA"),e
    write(30,"(a,1x,a,1x,a)")trim("SCALARS"),trim("error-map"),trim("double")
    write(30,"(a,1x,a)")trim("LOOKUP_TABLE"),trim("default")
    do i = 1 , e
      write(30,"(f14.12)")el2(i)
    enddo

    close (30)
  end subroutine print_text2
    
   subroutine gziita(i,c,a,nn1,nn2,nn3,nn4)
   implicit none
   integer i
   real(8) a,c,nn1,nn2,nn3,nn4
      if (i == 1)then
         a = -0.577350269189626d0
         c = -0.577350269189626d0
      else if (i == 2)then
         a = -0.577350269189626d0
         c = 0.577350269189626d0
      else if (i == 3)then
         a = 0.577350269189626d0
         c = 0.577350269189626d0
      else if (i == 4)then
         a = 0.577350269189626d0
         c = -0.577350269189626d0
      end if
      nn1 = (1.0d0-c)*(1.0d0-a)
      nn2 = (1.0d0+c)*(1.0d0-a)
      nn3 = (1.0d0+c)*(1.0d0+a)
      nn4 = (1.0d0-c)*(1.0d0+a)
   end subroutine

   subroutine Dmat(d,y,p)
   implicit none
   real(8) y,p,d(3,3)
      d(1,1) = y / (1.0d0 + p) / (1.0d0 - 2.0d0*p)*(1.0d0-p)
      d(1,2) = y / (1.0d0 + p) * p / (1.0d0 -2.0d0* p)
      d(1,3) = 0.0d0
      d(2,1) = y / (1.0d0 + p) * p / (1.0d0 - 2.0d0*p)
      d(2,2) = y / (1.0d0 + p) / (1.0d0 -2.0d0* p)*(1.0d0-p)
      d(2,3) = 0.0d0
      d(3,1) = 0.0d0
      d(3,2) = 0.0d0
      d(3,3) = 1.0d0 / 2.0d0 * y / (1.0d0 + p)
   end subroutine

   subroutine jmat(jm,c,a,n,globalnum,j)
   implicit none
   integer j
   real(8) jm(2,2),c,a
   integer,allocatable::globalnum(:,:)
   real(8),allocatable::n(:,:)
      jm(1,1) = ((-1.0d0 + a) * n(globalnum(j,1),1) +(1.0d0 - a) * n(globalnum(j,2),1) +&
               (1.0d0 + a) * n(globalnum(j,3),1) + (-1.0d0 - a) * n(globalnum(j,4),1)) / 4.0d0
      jm(2,1) = ((-1.0d0 + c) * n(globalnum(j,1),1) +(-1.0d0 - c) * n(globalnum(j,2),1) +&
               (1.0d0 + c) * n(globalnum(j,3),1) + (1.0d0 - c) * n(globalnum(j,4),1)) / 4.0d0
      jm(1,2) = ((-1.0d0 + a) * n(globalnum(j,1),2) +(1.0d0 - a) * n(globalnum(j,2),2) +&
               (1.0d0 + a) * n(globalnum(j,3),2) + (-1.0d0 - a) * n(globalnum(j,4),2)) / 4.0d0
      jm(2,2) = ((-1.0d0 + c) * n(globalnum(j,1),2) +(-1.0d0 - c) * n(globalnum(j,2),2) +&
               (1.0d0 + c) * n(globalnum(j,3),2) + (1.0d0 - c) * n(globalnum(j,4),2)) / 4.0d0
   end subroutine

   subroutine Bmat(bm,nxy,jmi,jm,c,a)
   implicit none
   real(8) jm(2,2),jmi(2,2),nxy(4,2),bm(3,8),c,a
      jmi(1,1) = jm(2,2) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
      jmi(1,2) = -jm(1,2) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
      jmi(2,1) = -jm(2,1) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
      jmi(2,2) = jm(1,1) / (jm(1,1) * jm(2,2) - jm(1,2) * jm(2,1))
   
      nxy(1,1) = (-(1.0d0 - a) * jmi(1,1) - (1.0d0 - c) * jmi(1,2))/ 4.0d0 
      nxy(1,2) = (-(1.0d0 - a) * jmi(2,1) - (1.0d0 - c) * jmi(2,2))/ 4.0d0 
      nxy(2,1) = ((1.0d0 - a) * jmi(1,1) - (1.0d0 + c) * jmi(1,2))/ 4.0d0 
      nxy(2,2) = ((1.0d0 - a) * jmi(2,1) - (1.0d0 + c) * jmi(2,2))/ 4.0d0
      nxy(3,1) = ((1.0d0 + a) * jmi(1,1) + (1.0d0 + c) * jmi(1,2))/ 4.0d0
      nxy(3,2) = ((1.0d0 + a) * jmi(2,1) + (1.0d0 + c) * jmi(2,2))/ 4.0d0
      nxy(4,1) = (-(1.0d0 + a) * jmi(1,1) + (1.0d0 - c) * jmi(1,2))/ 4.0d0
      nxy(4,2) = (-(1.0d0 + a) * jmi(2,1) + (1.0d0 - c) * jmi(2,2))/ 4.0d0
            
      bm(1,1) = nxy(1,1)
      bm(1,2) = 0.0d0
      bm(1,3) = nxy(2,1)
      bm(1,4) = 0.0d0
      bm(1,5) = nxy(3,1)
      bm(1,6) = 0.0d0
      bm(1,7) = nxy(4,1)
      bm(1,8) = 0.0d0
      bm(2,1) = 0.0d0
      bm(2,2) = nxy(1,2)
      bm(2,3) = 0.0d0
      bm(2,4) = nxy(2,2)
      bm(2,5) = 0.0d0
      bm(2,6) = nxy(3,2)
      bm(2,7) = 0.0d0
      bm(2,8) = nxy(4,2)
      bm(3,1) = nxy(1,2)
      bm(3,2) = nxy(1,1)
      bm(3,3) = nxy(2,2)
      bm(3,4) = nxy(2,1)
      bm(3,5) = nxy(3,2)
      bm(3,6) = nxy(3,1)
      bm(3,7) = nxy(4,2)
      bm(3,8) = nxy(4,1)
   end subroutine

   subroutine uthdata(b,a1,y1,y2,p1,p2,b1,alpha)
   implicit none
   real(8) b1,b,lamda1,lamda2,mu1,mu2,y1,y2,p1,p2,alpha,a1
      b1 = b * sqrt(2.0d0)
      lamda1 = y1*p1 / (1.0d0 + p1)/(1.0d0-2.0d0*p1)
      lamda2 = y2*p2 / (1.0d0 + p2)/(1.0d0-2.0d0*p2)
      mu1 = y1 / 2.0d0 /(1.0d0 + p1)
      mu2 = y2 / 2.0d0 /(1.0d0 + p2)
      alpha = ((lamda1 + mu1 + mu2) * b1 * b1) /((lamda2 + mu2) *a1*a1 +(lamda1 + mu1)*(b1*b1 - a1*a1) + mu2 *b1*b1)
   end subroutine

   subroutine uth(a1,b1,alpha,xi,yi,uthx,uthy)
   implicit none
   real(8) xi,yi,r,sita,uthr,uthx,uthy,alpha,a1,b1
      r = sqrt(xi ** 2.0d0 + yi ** 2.0d0)
      sita = atan2(yi, xi)
      if (r < 3.0001d0) then  !!!中心からの距離で半径方向の理論解を分けて定義
         uthr = ((1.0d0 - b1 * b1 / a1 / a1) * alpha + b1 * b1 / a1 / a1) * r
      else
         uthr = (r-b1*b1/r)*alpha + b1*b1/r
      end if
      uthx = uthr * cos(sita) !x方向とy方向に分解
      uthy = uthr * sin(sita)
   end subroutine

end module 