program main
  implicit none
  character dummy
  integer i,j,e,ep,bc1,bc2,bc3,bc0,gr1,gr2
  integer,allocatable::globalnum(:,:)
  integer(8) pn
  real(8) alpha,lamda1,lamda2,mu1,mu2,sita,a,b,y1,p1,y2,p2
  real(8),allocatable::x(:),y(:),ux(:),uy(:),ur(:),r(:)
  a = 3.0d0
  b = 6.0d0! * sqrt(2.0d0)
  open(12, file="inclusion2", status="old")
    read (12, *)dummy
    read (12, *)dummy
    read (12, *)dummy
    read (12, *)dummy
    read (12, *)dummy, pn, dummy
    allocate (x(pn),y(pn), source = 0.0d0)
    do i = 1, pn
       read(12, *)x(i),y(i),dummy
    end do
    read (12, *)dummy, e, dummy
    ep = 4
    allocate (globalnum(e,ep), source = 0)
    do i = 1, e
      read(12, *)dummy,globalnum(i,1),globalnum(i,2),globalnum(i,3),globalnum(i,4)
    enddo

    read(12, *)dummy, e
    do i = 1, e
      read(12,*) dummy
    enddo

    read(12,*)dummy, e
    read(12,*) dummy,dummy,dummy,dummy
    read(12,*) dummy, dummy
    do i = 1, e
      read(12,*) j
      if(j == 15)then
        gr1 = i
      else if(j == 16)then
        gr2 = i
      endif
    enddo
    close (12)

    open(30, file = "nodeinclusion.dat", status = "replace")
    write(30,*) pn
    do i = 1, pn
      write(30,*)x(i),y(i)
    end do
    close (30)

    open(31, file = "eleminclusion.dat", status = "replace")
    write(31,*) e,ep
    do i = 1, e
      write(31,*)globalnum(i,1)+1,globalnum(i,2)+1,globalnum(i,3)+1,globalnum(i,4)+1
    end do
    close (31)

open(33, file = "inputinclusion.dat", status = "replace")
y1 = 10000.0d0
p1 = 0.25d0
y2 = 100000.0d0
p2 = 0.30d0
write(33,*) y1, p1
write(33,*) y2, p2
write(33,*) x(3), x(5)
close(33)

open(32, file = "bcinclusion.dat", status = "replace")

    lamda1 = y1*p1 / (1.0d0 + p1)/(1.0d0-2*p1)
    lamda2 = y2*p2 / (1.0d0 + p2)/(1.0d0-2*p2)
    mu1 = y1 / 2.0d0 /(1.0d0 + p1)
    mu2 = y2 / 2.0d0 /(1.0d0 + p2)
    alpha = (lamda1 + mu1 + mu2) * b * b /((lamda2 + mu2) *a*a +(lamda1 + mu1)*(b*b - a*a) + mu2 *b*b)
    allocate (r(pn),ur(pn),ux(pn),uy(pn),source = 0.0d0)

   do i = 1, pn
     r(i) = sqrt(x(i) ** 2.0d0 + y(i) ** 2.0d0)
     if(r(i) < 3.001d0) then
       ur(i) = ((1.0d0 - b * b / a / a) * alpha + b * b / a / a) * r(i)
     else
       ur(i) = (r(i)-b*b/r(i))*alpha + b*b/r(i)
     end if
     sita = atan2(y(i), x(i))
     ux(i) = ur(i) * cos(sita)
     uy(i) = ur(i) * sin(sita)
   end do

bc1 = 0
bc2 = 0
bc3 = 0
    do i = 1,pn
      if(y(i) == 0.0d0 .and. r(i) < 5.999d0)then
         bc1 = bc1 + 1
      end if
      if(x(i) == 0.0d0 .and. r(i) < 5.999d0)then
         bc2 = bc2 + 1
      end if
      if(r(i) > 5.999d0)then
         bc3 = bc3 + 1
      end if
    enddo

bc0 = bc1 + bc2 + bc3 * 2 
    write(32,*) bc0, 2, bc1, bc2, bc3
    
do i = 1, pn
if(x(i) == 0.0d0 .and. r(i) < 5.999d0)then
write(32,*) i, 1, 0.0d0
endif
if(y(i) == 0.0d0 .and. r(i) < 5.999d0 )then
write(32,*) i, 2, 0.0d0
endif
if(r(i) >= 5.999d0) then
write(32,*) i, 1, ux(i)
write(32,*) i, 2, uy(i) 
endif
end do
close (32)

open(34, file = "materialinclusion.dat", status = "replace")
do i = 1, gr1
  write(34,*)1
enddo
do i = gr1+1, gr2
  write(34,*)2
enddo
close(34)

end program