module module2
use module
  implicit none

contains
  subroutine Kesub(j,globalnum, n, y,p,d,jm,jmi,bm,Ke)
   implicit none
   integer,allocatable::globalnum(:,:)
   integer j,pn,ep,i,k,l,o
   real(8):: a, c, det, p, y,nn1,nn2,nn3,nn4
   real(8):: bdbdetp(8,8), bdb(8,8), db(3,8), jm(2,2), bt(8,3) = 0.0d0,&
             d(3,3), jmi(2,2), nxy(4,2) = 0.0d0, bm(3,8), Ke(8,8)
   real(8), allocatable ::n(:,:)
   
   Ke = 0.0d0
   do i = 1, 4
      bdbdetp = 0.0d0
      bdb = 0.0d0
      db = 0.0d0

      call Dmat(d,y,p)
      call gziita(i,c,a,nn1,nn2,nn3,nn4)
      call jmat(jm,c,a,n,globalnum,j)
      call Bmat(bm,nxy,jmi,jm,c,a)

      do o = 1, 3
         do q = 1, 8
            bt(q,o) = bm(o,q) !btを定義
         end do
      end do
         
      do o = 1, 3
         do q = 1, 8
            do w = 1, 3
               db(o,q) = db(o,q) + d(o,w) * bm(w,q) !dbを計算
            end do
         end do
      end do
         
      do o = 1, 8
         do q = 1, 8
            do w = 1, 3
               bdb(o,q) = bdb(o,q) + bt(o,w) * db(w,q) !bdbを計算
            end do
         end do
      end do
 
      det = (jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)) !det計算

      if(det < 0) then
         write(*,*) "det<0"
         stop 
      end if
              
      do o = 1, 8
         do q = 1, 8
            bdbdetp(q,o) = bdb(q,o) * det !bdbdetp計算
         end do
      end do

      do k = 1,8
         do l = 1,8
            Ke(k,l) = Ke(k,l) + bdbdetp(k,l)
         end do
      end do
   end do
end subroutine Kesub

end module 