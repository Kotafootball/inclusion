program main
use module
implicit none

integer j,e,pn,ep,f1,bc0,aa,k,l,g
integer,allocatable::globalnum(:,:),bc(:,:)
real(8) r,det,ec,em,sita,a,c,a1,b1,y1,y2,xi,yi,p1,p2,b,gzi,ita,nn1,nn2,nn3,nn4,xxx,yyy!,el2
real(8) jm(2,2),lamda1,lamda2,mu1,mu2,alpha,uthr,uthx,uthy,uxfem,uyfem
real(8),allocatable:: n(:,:),u(:),bc1(:),el2(:)
aa = 1
em = 0.0d0
ec = 0.0d0
call read_file1(y1,p1,y2,p2,a1,b) 
call read_file2(pn, n)
call read_file3(e, ep, globalnum) 
call read_file4(bc0 ,f1, bc, bc1)
call read_file6(u,pn,f1)
b1 = b * sqrt(2.0d0)
lamda1 = y1*p1 / (1.0d0 + p1)/(1.0d0-2.0d0*p1)
lamda2 = y2*p2 / (1.0d0 + p2)/(1.0d0-2.0d0*p2)
mu1 = y1 / 2.0d0 /(1.0d0 + p1)
mu2 = y2 / 2.0d0 /(1.0d0 + p2)
alpha = (lamda1 + mu1 + mu2) * b1 * b1 /((lamda2 + mu2) *a1*a1 +(lamda1 + mu1)*(b1*b1 - a1*a1) + mu2 *b1*b1)

allocate(el2(e))
do i = 1, e  !要素ループ
ec = 0.0d0
em = 0.0d0
   do k = 1, aa
      do l = 1, aa
         xxx = 2.0d0*real(l)/real(aa) - 1.0d0  !!各格子の右上の点の座標
         yyy = 2.0d0*real(k)/real(aa) - 1.0d0
         do j = 1,4  !積分点ループ
            if (j == 1)then
               a = -0.577350269189626d0
               c = -0.577350269189626d0
            else if (j == 2)then
               a = -0.577350269189626d0
               c = 0.577350269189626d0
            else if (j == 3)then
               a = 0.577350269189626d0
               c = 0.577350269189626d0
            else if (j == 4)then
               a = 0.577350269189626d0
               c = -0.577350269189626d0
            end if
            nn1 = (1.0d0-c)*(1.0d0-a)
            nn2 = (1.0d0+c)*(1.0d0-a)
            nn3 = (1.0d0+c)*(1.0d0+a)
            nn4 = (1.0d0-c)*(1.0d0+a)
            gzi = (nn1*(xxx-2.0d0/real(aa)) + nn2*xxx + nn3*xxx + nn4*(xxx-2.0d0/real(aa)))/4.0d0
            ita = (nn1*(yyy-2.0d0/real(aa)) + nn2*(yyy-2.0d0/real(aa)) + nn3*yyy + nn4*yyy)/4.0d0

            nn1 = (1.0d0-gzi)*(1.0d0-ita) !!格子点内部の積分点形状関数
            nn2 = (1.0d0+gzi)*(1.0d0-ita)
            nn3 = (1.0d0+gzi)*(1.0d0+ita)
            nn4 = (1.0d0-gzi)*(1.0d0+ita)
            xi = (nn1*n(globalnum(i,1),1) + nn2*n(globalnum(i,2),1) + &
               nn3*n(globalnum(i,3),1) + nn4*n(globalnum(i,4),1)) / 4.0d0
            yi = (nn1*n(globalnum(i,1),2) + nn2*n(globalnum(i,2),2) + &
               nn3*n(globalnum(i,3),2) + nn4*n(globalnum(i,4),2)) / 4.0d0
            r = sqrt(xi ** 2.0d0 + yi ** 2.0d0)
            sita = atan2(yi, xi)
            if (r < 3.0001d0) then  !!!中心からの距離で半径方向の理論解を分けて定義
               uthr = ((1.0d0 - b1 * b1 / a1 / a1) * alpha + b1 * b1 / a1 / a1) * r
            else
               uthr = (r-b1*b1/r)*alpha + b1*b1/r
            end if
            uthx = uthr * cos(sita) !x方向とy方向に分解
            uthy = uthr * sin(sita)

            uxfem = (nn1*u(globalnum(i,1)*2-1) + nn2*u(globalnum(i,2)*2-1) + &  !!積分点変位
               nn3*u(globalnum(i,3)*2-1) + nn4*u(globalnum(i,4)*2-1)) / 4.0d0
            uyfem = (nn1*u(globalnum(i,1)*2) + nn2*u(globalnum(i,2)*2) + &
               nn3*u(globalnum(i,3)*2) + nn4*u(globalnum(i,4)*2)) / 4.0d0

            jm(1,1) = ((-1.0d0 + a) * n(globalnum(i,1),1) +(1.0d0 - a) * n(globalnum(i,2),1) +&
               (1.0d0 + a) * n(globalnum(i,3),1) + (-1.0d0 - a) * n(globalnum(i,4),1)) / 4.0d0
            jm(2,1) = ((-1.0d0 + c) * n(globalnum(i,1),1) +(-1.0d0 - c) * n(globalnum(i,2),1) +&
               (1.0d0 + c) * n(globalnum(i,3),1) + (1.0d0 - c) * n(globalnum(i,4),1)) / 4.0d0
            jm(1,2) = ((-1.0d0 + a) * n(globalnum(i,1),2) +(1.0d0 - a) * n(globalnum(i,2),2) +&
               (1.0d0 + a) * n(globalnum(i,3),2) + (-1.0d0 - a) * n(globalnum(i,4),2)) / 4.0d0
            jm(2,2) = ((-1.0d0 + c) * n(globalnum(i,1),2) +(-1.0d0 - c) * n(globalnum(i,2),2) +&
               (1.0d0 + c) * n(globalnum(i,3),2) + (1.0d0 - c) * n(globalnum(i,4),2)) / 4.0d0

            det = (jm(1,1) * jm(2,2)) - (jm(1,2) * jm(2,1)) /real(aa)/real(aa)
            ec = ec + ((uthx - uxfem) ** 2.0d0 + (uthy - uyfem) ** 2.0d0) * det  
            em = em + (uthx ** 2.0d0 + uthy ** 2.0d0) * det
         end do
      end do
   end do
   el2(i) = sqrt(ec) / sqrt(em)
end do

!el2 = sqrt(ec) / sqrt(em)
!write(*,*) "error", el2
call print_text2(g,ep,e,pn,globalnum,n,el2)
end program