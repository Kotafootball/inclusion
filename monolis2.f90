program main
use module
use mod_monolis
implicit none

integer j, Nn, bc0, f1, e, ep, ld0, f2, g, pn,l,aa
integer,allocatable::globalnum(:,:),bc(:,:),ld(:,:),connectivity(:),globalnumt(:,:)
real(8), allocatable::u(:), f(:),n(:,:),bc1(:),ldf(:),eps(:,:),sgm(:,:),el2(:)
real(8) ::bm(3,8) = 0.0d0,d(3,3) = 0.0d0,jm(2,2) = 0.0d0,jmi(2,2) = 0.0d0,Ke(8,8) = 0.0d0
real(8) y, p,t1,t2,y1,p1,y2,p2,a,b,el22
type(monolis_structure) :: K ! monolis 構造体 Ax = b の疎⾏列相当

call cpu_time(t1)

call read_file1(y1,p1,y2,p2,a,b) 
call read_file2(pn, n)
call read_file3(e, ep, globalnum) 
call read_file4(bc0 ,f1, bc, bc1)

Nn = pn * f1
allocate(u(Nn), f(Nn), source = 0.0d0)
allocate(eps(3,e),sgm(3,e), source = 0.0d0)
allocate(connectivity(ep),source = 0)
allocate(globalnumt(ep,e), source = 0)

!monolis
call monolis_global_initialize() ! monolis の全体初期化
call monolis_initialize(K, "./") ! monolis の初期化
!> この部分で疎⾏列 A の構成、求解が可能
do i = 1, e
  do j = 1, ep
    globalnumt(j,i) = globalnum(i,j)
  end do
end do

call monolis_get_nonzero_pattern(K, pn, ep, f1, e, globalnumt) ! 疎⾏列パターンの決定
aa = 1
open(30, file="materialinclusion.dat", status="old")
do i = 1, e
read(30,*)l
  if(l == 1) then
    y = y1
    p = p1
  else if(l == 2) then
    y = y2
    p = p2
  end if
  call KG(aa,i,globalnum, n, y,p,d,jm,jmi,bm,Ke)
  !call Kesub(i,globalnum, n, y,p,d,jm,jmi,bm,Ke)
  do j = 1, ep
    connectivity(j) = globalnum(i,j) 
  end do
  call monolis_add_matrix_to_sparse_matrix(K, ep, connectivity, Ke)! ⾏列成分の⾜し込み
end do
close(30)

do i = 1, bc0
  call monolis_set_Dirichlet_bc(K, f, bc(i,1), bc(i,2), bc1(i)) ! Dirichlet 境界条件の付与
end do

call monolis_param_set_maxiter(K, 10000) !> 反復法の最大反復回数を 10000 回に設定
call monolis_param_set_precond(K, monolis_prec_SOR)
call monolis_param_set_is_debug(K, .true.)
call monolis_solve(K,f,u) ! 求解 --> ** monolis solver: CG, prec: Diag

call monolis_finalize(K) ! monolis の終了処理
call monolis_global_finalize() ! monolis の全体終了処理
!monolis finish

call error(n,pn,e,globalnum,u,el22)
open(31, file = "dispinclusion.dat", status = "replace")
    do i = 1, Nn
    write(31,*) u(i)
    enddo
close(31)
call print_text(g,ep,e,pn,globalnum,n,u,eps,sgm)
write(*,*) " "
write(*,*) "error", el22
call cpu_time(t2)

write(*,*) "time", t2 - t1 ,"sec"
write(*,*)"e",e, "dof", pn*f1

end program